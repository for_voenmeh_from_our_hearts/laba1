#include "factorial.h"

int factorial(int n) {
	int result;
	if (!std::cin || n < 0 || n>100)
		return -1;

	result = 1;

	for (int i = 1; i <= n; i++)
	{
		result = result * i;
	}
	return result;
}