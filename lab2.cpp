#include <iostream>
#include "factorial.h"

using namespace std;

int main()
{
	int n, result;
	//cout << "input number: ";
	//cin >> n;
	n=4;
	result = factorial(n);
	if (result == -1) cout << "error\n";
	else cout << n << "! = " << result << endl;
}