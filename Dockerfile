FROM ubuntu:latest

COPY ./*.deb /lab/

RUN apt-get update && apt-get install -y /lab/*.deb 

CMD ["/bin/bash"]
