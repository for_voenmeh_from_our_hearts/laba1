#include "factorial.h"
using namespace std;

int main()
{
	//тест 1 - проверка на правильность значений
	int test = 0;
	if (factorial(0) == 1) test++;
	if (factorial(1) == 1) test++;
	if (factorial(4) == 24) test++;
	if (factorial(9) == 362880) test++;
	if (factorial(3) == 6) test++;

	if (test == 5) cout << "test #1 completed\n";

	//тест 2 - проверка обработки букв
	if (factorial('m') == -1) test--;
	if (factorial('j') == -1) test--;
	if (factorial('f') == -1) test--;
	if (factorial('p') == -1) test--;
	if (factorial('/') == -1) test--;

	if (test == 0) cout << "test #2 completed\n";

	//тест 3 - проверка значений на диапазон
	test = 5;
	if (factorial(-78) == -1) test--;
	if (factorial(2323) == -1) test--;
	if (factorial(-8) == -1) test--;
	if (factorial(101) == -1) test--;
	if (factorial(-1) == -1) test--;

	if (test == 0) cout << "test #3 completed\n";
}
